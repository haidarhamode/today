//
//  EKReminder+Reminder.swift
//  Today
//
//  Created by Haidar on 8/26/23.
//

import EventKit
import Foundation


extension EKReminder {
    func update(using reminder: Reminder, in store: EKEventStore) {
        title = reminder.title
        notes = reminder.notes
        isCompleted = reminder.isComplete
        // EventKit calendar items must be associated with a calendar.
        calendar = store.defaultCalendarForNewReminders()
        /*
         Iterate through the alarms, removing any alarm that doesn’t correspond to the reminder’s due date.
         The comparison determines the dates to be the same if they occur during the same minute.
         */
        alarms?.forEach { alarm in
            guard let absoluteDate = alarm.absoluteDate else { return }
            let comparison = Locale.current.calendar.compare(
                reminder.dueDate, to: absoluteDate, toGranularity: .minute)
            if comparison != .orderedSame {
                removeAlarm(alarm)
            }
        }
        // If the reminder has no alarms, add an alarm for the due date.
        if !hasAlarms {
            addAlarm(EKAlarm(absoluteDate: reminder.dueDate))
        }
    }
}
