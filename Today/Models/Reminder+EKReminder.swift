//
//  Reminder+EKReminder.swift
//  Today
//
//  Created by Haidar on 8/26/23.
//

import EventKit
import Foundation


extension Reminder {
    init(with ekReminder: EKReminder) throws {
        /*
         A guard statement that binds the absolute date of a
         reminder’s first alarm. In the else clause, throw an error.
         */
        guard let dueDate = ekReminder.alarms?.first?.absoluteDate else {
            throw TodayError.reminderHasNoDueDate
        }
        id = ekReminder.calendarItemIdentifier
        title = ekReminder.title
        self.dueDate = dueDate
        notes = ekReminder.notes
        isComplete = ekReminder.isCompleted
    }
}
