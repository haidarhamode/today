//
//  CAGradientLayer+ListStyle.swift
//  Today
//
//  Created by Haidar on 8/26/23.
//

import UIKit

/*
 - UIKit provides the CAGradientLayer class to represent a
 color gradient. You’ll extend this class to create color gradients
 for stylizing the list of reminders.
 
 - UIKit includes the class CAGradientLayer that represents a
 color gradient. You create a gradient by providing any number of
 colors in an array. By default, the system draws the colors uniformly
 across the layer, but you can define specific locations for the gradient stops.
 */
extension CAGradientLayer {
    /*
     Because you declare the function in an extension on CAGradientLayer,
     the capitalized Self in the return type refers to the CAGradientLayer type.
     */
    static func gradientLayer(for style: ReminderListStyle, in frame: CGRect) -> Self {
        let layer = Self()
        layer.colors = colors(for: style)
        layer.frame = frame
        return layer
    }
    
    private static func colors(for style: ReminderListStyle) -> [CGColor] {
        // These constants will define the starting and ending colors for the gradient stops.
        let beginColor: UIColor
        let endColor: UIColor
        
        switch style {
        case .all:
            beginColor = .todayGradientAllBegin
            endColor = .todayGradientAllEnd
        case .future:
            beginColor = .todayGradientFutureBegin
            endColor = .todayGradientFutureEnd
        case .today:
            beginColor = .todayGradientTodayBegin
            endColor = .todayGradientTodayEnd
        }
        return [beginColor.cgColor, endColor.cgColor]
    }
}
