//
//  ReminderStore.swift
//  Today
//
//  Created by Haidar on 8/26/23.
//

import EventKit
import Foundation

/*
 You can’t override methods in final classes.
 The compiler will display a warning if you try to subclass ReminderStore.
 */
final class ReminderStore {
    static let shared = ReminderStore()
    
    private let ekStore = EKEventStore()
    
    var isAvailable: Bool {
        EKEventStore.authorizationStatus(for: .reminder) == .authorized
    }
    
    func requestAccess() async throws {
        let status = EKEventStore.authorizationStatus(for: .reminder)
        switch status {
        case .authorized:
            return
        case .restricted:
            throw TodayError.accessRestricted
        case .notDetermined:
            /*
             Request access to user data if the user has not already made a decision.
             */
            let accessGranted = try await ekStore.requestAccess(to: .reminder)
            guard accessGranted else {
                throw TodayError.accessDenied
            }
        case .denied:
            throw TodayError.accessDenied
        @unknown default:
            throw TodayError.unknown
        }
    }
    
    func readAll() async throws -> [Reminder] {
        guard isAvailable else {
            throw TodayError.accessDenied
        }
        /*
         This predicate narrows the results to only reminder items.
         If you choose, you can further narrow the results to reminders from specific calendars.
         */
        let predicate = ekStore.predicateForReminders(in: nil)
        let ekReminders = try await ekStore.reminders(matching: predicate)
        /*
         compactMap(_:) works as both a filter and a map, allowing you to
         discard items from the source collection.
         */
        let reminders: [Reminder] = try ekReminders.compactMap { ekReminder in
            do {
                return try Reminder(with: ekReminder)
            } catch TodayError.reminderHasNoDueDate {
                return nil
            }
        }
        return reminders
    }
    
    /*
     You won’t use the identifier that this method returns in all situations.
     The @discardableResult attribute instructs the compiler to omit warnings
     in cases where the call site doesn’t capture the return value.
     */
    @discardableResult
    func save(_ reminder: Reminder) throws -> Reminder.ID {
        guard isAvailable else {
            throw TodayError.accessDenied
        }
        let ekReminder: EKReminder
        do {
            ekReminder = try read(with: reminder.id)
        } catch {
            /*
             Failing to find a reminder with the corresponding identifier
             doesn’t indicate an error. Rather, it indicates that
             the user is saving a new reminder.
             */
            ekReminder = EKReminder(eventStore: ekStore)
        }
        ekReminder.update(using: reminder, in: ekStore)
        try ekStore.save(ekReminder, commit: true)
        return ekReminder.calendarItemIdentifier
    }
    
    
    
    func remove(with id: Reminder.ID) throws {
        guard isAvailable else {
            throw TodayError.accessDenied
        }
        let ekReminder = try read(with: id)
        try ekStore.remove(ekReminder, commit: true)
    }
    
    /*
     EventKit contains all data from a user’s calendar, not just reminder data.
     You’ll query for a calendar item that matches your reminder identifier.
     */
    private func read(with id: Reminder.ID) throws -> EKReminder {
        /*
         Because the method calendarItem(withIdentifier:) returns the
         superclass EKCalendarItem, you downcast to EKReminder.
         The downcast ensures that you can safely treat the item as an EKReminder.
         */
        guard let ekReminder = ekStore.calendarItem(withIdentifier: id) as? EKReminder else {
            throw TodayError.failedReadingCalendarItem
        }
        return ekReminder
    }
}
