//
//  ReminderListStyle.swift
//  Today
//
//  Created by Haidar on 8/25/23.
//

import Foundation

/*
 Because the enumeration stores raw Int values, Swift automatically
 assigns each case a number, starting from 0.
 */
enum ReminderListStyle: Int {
    case today
    case future
    case all
    
    var name: String {
        switch self {
        case .today:
            return NSLocalizedString("Today", comment: "Today style name")
        case .future:
            return NSLocalizedString("Future", comment: "Future style name")
        case .all:
            return NSLocalizedString("All", comment: "All style name")
        }
    }
    
    func shouldInclude(date: Date) -> Bool {
        /*
         The value of isInToday is true if the date that the caller passes to the
         function is today and is false if it is not. Locale.current.calendar is the current
         calendar based on the user’s region settings.
         */
        let isInToday = Locale.current.calendar.isDateInToday(date)
        switch self {
        case .today:
            return isInToday
        case .future:
            return (date > Date.now) && !isInToday
        case .all:
            return true
        }
    }
}
