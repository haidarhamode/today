//
//  EKEventStore+AsyncFetch.swift
//  Today
//
//  Created by Haidar on 8/26/23.
//

import EventKit
import Foundation

// EKEventStore objects can access a user’s calendar events and reminders.
extension EKEventStore {
    func reminders(matching predicate: NSPredicate) async throws -> [EKReminder] {
        /*
         Suspends the current task, then calls the given closure
         with a checked throwing continuation for the current task.
         */
        try await withCheckedThrowingContinuation { continuation in
            // The method passes the matching reminders to the completion handler.
            fetchReminders(matching: predicate) { reminders in
                if let reminders {
                    /*
                     Upon success, resume the continuation, returning the reminders.
                     Your function resumes execution and returns the EKReminder array.
                     */
                    continuation.resume(returning: reminders)
                } else {
                    continuation.resume(throwing: TodayError.failedReadingReminders)
                }
            }
        }
    }
}
