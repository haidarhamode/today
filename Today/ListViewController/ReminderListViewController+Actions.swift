//
//  ReminderListViewController+Actions.swift
//  Today
//
//  Created by Haidar on 8/20/23.
//

import UIKit

extension ReminderListViewController{
    /*
     The @objc attribute makes this method available to Objective-C code.
     In the next section, you’ll use an Objective-C API to attach
     the method to your custom button.
     */
    
    @objc func eventStoreChanged(_ notification: NSNotification) {
        reminderStoreChanged()
    }
    @objc func didPressDoneButton(_ sender: ReminderDoneButton) {
        guard let id = sender.id else { return }
        completeReminder(withId: id)
    }
    
    @objc func didPressAddButton(_ sender: UIBarButtonItem) {
        let reminder = Reminder(title: "", dueDate: Date.now)
        /*
         Add [weak self] to the closure’s capture list to prevent the
         reminder view controller from capturing and storing a strong reference
         to the reminder list view controller.
         */
        let viewController = ReminderViewController(reminder: reminder) { [weak self] reminder in
            self?.addReminder(reminder)
            self?.updateSnapshot()
            // Dismiss the view controller.
            self?.dismiss(animated: true)
        }
        viewController.isAddingNewReminder = true
        viewController.setEditing(true, animated: false)
        /*
         You can specify didCancelAdd as the selector because you
         marked the method with the @objc attribute.
         */
        viewController.navigationItem.leftBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .cancel, target: self, action: #selector(didCancelAdd(_:)))
        viewController.navigationItem.title = NSLocalizedString(
            "Add Reminder", comment: "Add Reminder view controller title")
        let navigationController = UINavigationController(rootViewController: viewController)
        present(navigationController, animated: true)
    }
    
    
    @objc func didCancelAdd(_ sender: UIBarButtonItem) {
        dismiss(animated: true)
    }
    
    @objc func didChangeListStyle(_ sender: UISegmentedControl) {
        listStyle = ReminderListStyle(rawValue: sender.selectedSegmentIndex) ?? .today
        updateSnapshot()
        refreshBackground()
    }
}
