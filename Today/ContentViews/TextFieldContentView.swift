//
//  TextFieldContentView.swift
//  Today
//
//  Created by Haidar on 8/24/23.
//

import UIKit


class TextFieldContentView: UIView , UIContentView{
    struct Configuration: UIContentConfiguration {
        var text: String? = ""
        /*
         This empty closure holds the behavior that you’d like to
         perform when the user edits the text in the text field.
         */
        var onChange: (String) -> Void = { _ in }
        
        func makeContentView() -> UIView & UIContentView {
            return TextFieldContentView(self)
        }
    }
    
    let textField = UITextField()
    
    var configuration: UIContentConfiguration {
        // Add a didSet observer that calls the new configure method to the configuration property.
        didSet {
            configure(configuration: configuration)
        }
    }
    
    /*
     The system assigns an intrinsic content size—a width and height determined by what it displays—to every subclass of UIView.
     Override the intrinsic content size to fix the height at 44 points, the minimum size for an accessible control.
     */
    override var intrinsicContentSize: CGSize {
        CGSize(width: 0, height: 44)
    }
    
    init(_ configuration: UIContentConfiguration) {
        self.configuration = configuration
        super.init(frame: .zero)
        // pin the text field, and then provide horizontal padding insets.
        addPinnedSubview(textField, insets: UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16))
        /*
         By adding a target and action to this view, the view calls
         the target’s didChange(_:) selector whenever the control detects a user interaction.
         In this case, you invoke the method whenever a user changes the text in the field.
        */
        textField.addTarget(self, action: #selector(didChange(_:)), for: .editingChanged)
        /*
         This property directs the text field to display a Clear Text button
         on its trailing side when it has contents, providing a way for the user to remove text quickly.
         */
        textField.clearButtonMode = .whileEditing
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(configuration: UIContentConfiguration) {
        guard let configuration = configuration as? Configuration else { return }
        textField.text = configuration.text
    }
    
    @objc private func didChange(_ sender: UITextField) {
        guard let configuration = configuration as? TextFieldContentView.Configuration else { return }
        configuration.onChange(textField.text ?? "")
    }
}


extension UICollectionViewListCell {
    func textFieldConfiguration() -> TextFieldContentView.Configuration {
        TextFieldContentView.Configuration()
    }
}
