//
//  ReminderViewController+Section.swift
//  Today
//
//  Created by Haidar on 8/23/23.
//

import Foundation


extension ReminderViewController {
    /*
     Section and item identifier types must conform to Hashable because
     the data source uses hash values to determine changes in your data.
     */
    enum Section: Int, Hashable {
        case view
        case title
        case date
        case notes
        
        
        var name: String {
            switch self {
            case .view: return ""
            case .title:
                return NSLocalizedString("Title", comment: "Title section name")
            case .date:
                return NSLocalizedString("Date", comment: "Date section name")
            case .notes:
                return NSLocalizedString("Notes", comment: "Notes section name")
            }
        }
    }
    
}
