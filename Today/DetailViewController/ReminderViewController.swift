//
//  ReminderViewController.swift
//  Today
//
//  Created by Haidar on 8/21/23.
//

import UIKit


class ReminderViewController: UICollectionViewController {
    /*
     Data sources are generic. By specifying Int and Row generic parameters, you instruct the compiler that
     your data source uses instances of Section for the section numbers and instances of Row—the custom
     enumeration that you defined in the previous section—for the list rows.
     */
    private typealias DataSource = UICollectionViewDiffableDataSource<Section, Row>
    /*
     Like data sources, snapshots are generic.
     By specifying Int and Row generic parameters, you instruct the compiler that your snapshots
     use Section instances for section numbers and Row instances for items in the list.
     */
    private typealias Snapshot = NSDiffableDataSourceSnapshot<Section, Row>
    
    var reminder: Reminder {
        didSet {
            onChange(reminder)
        }
    }
    var workingReminder: Reminder
    var isAddingNewReminder = false
    var onChange: (Reminder) -> Void
    private var dataSource: DataSource!
    /*
     A closure is said to escape a function when the closure is passed as an argument to the
     function, but is called after the function returns. When you declare a function that
     takes a closure as one of its parameters, you can write @escaping before the parameter’s type
     to indicate that the closure is allowed to escape.
     */
    init(reminder: Reminder, onChange:  @escaping (Reminder) -> Void) {
        self.reminder = reminder
        self.workingReminder = reminder
        self.onChange = onChange
        var listConfiguration = UICollectionLayoutListConfiguration(appearance: .insetGrouped)
        listConfiguration.showsSeparators = false
        listConfiguration.headerMode = .firstItemInSection
        /*
         UIKit provides compositional layout classes that describe layouts for common applications, such as simple lists,
         grouped lists, and grids. You’ll display the details of a reminder in a simple list.
         */
        let listLayout = UICollectionViewCompositionalLayout.list(using: listConfiguration)
        super.init(collectionViewLayout: listLayout)
    }
    
    /*
     Interface Builder stores archives of the view controllers you create.
     A view controller requires an init(coder:) initializer so the system can initialize it using such an archive.
     If the view controller can’t be decoded and constructed, the initialization fails.
     When constructing an object using a failable initializer, the result is an optional
     that contains either the initialized object if it succeeds or nil if the initialization fails.
     */
    // The failable initializer that NSCoding requires.
    /*
     By including init(coder:), you satisfy the requirement.
     Because you create reminder view controllers in Today only in code, the app never uses this initializer.
     */
    required init?(coder: NSCoder) {
        fatalError("Always initialize ReminderViewController using init(reminder:)")
    }
    
    override func viewDidLoad() {
        // You first give the superclass a chance to perform its own tasks prior to your custom tasks.
        super.viewDidLoad()
        let cellRegistration = UICollectionView.CellRegistration(handler: cellRegistrationHandler)
        dataSource = DataSource(collectionView: collectionView) {
            (collectionView: UICollectionView, indexPath: IndexPath, itemIdentifier: Row) in
            return collectionView.dequeueConfiguredReusableCell(
                using: cellRegistration, for: indexPath, item: itemIdentifier)
        }
        if #available(iOS 16, *) {
            /*
             The .navigator style places the title in the center horizontally and includes a back button on the left.
             */
            navigationItem.style = .navigator
        }
        navigationItem.title = NSLocalizedString("Reminder", comment: "Reminder view controller title")
        /*
         You apply the data snapshot to this list the first time that the view controller loads.
         Later, when you’re editing the reminder detail items, you’ll need to apply another
         snapshot to update the user interface so that it reflects any changes the user makes.
         */
        //  Tapping an Edit button automatically toggles its title between “Edit” and “Done.”
        navigationItem.rightBarButtonItem = editButtonItem
        
        updateSnapshotForViewing()
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        if editing {
            prepareForEditing()
        } else {
            if isAddingNewReminder {
                onChange(workingReminder)
            } else {
                prepareForViewing()
            }
        }
    }
    
    func cellRegistrationHandler(cell: UICollectionViewListCell, indexPath: IndexPath, row: Row) {
        let section = section(for: indexPath)
        /*
         Add a switch statement using a tuple to configure cells for different section and row combinations.
         You use a tuple to group section and row values into a single compound value that
         you can use with the switch statement.
         */
        switch (section, row) {
            // a case that matches a header row, and store the header row’s associated String value in a constant named title.
        case (_, .header(let title)):
            cell.contentConfiguration = headerConfiguration(for: cell, with: title)
            // The underscore character (_) is a wildcard that matches any row value.
        case (.view, _):
            cell.contentConfiguration = defaultConfiguration(for: cell, at: row)
        case (.title, .editableText(let title)):
            cell.contentConfiguration = titleConfiguration(for: cell, with: title)
        case (.date, .editableDate(let date)):
            cell.contentConfiguration = dateConfiguration(for: cell, with: date)
        case (.notes,.editableText(let notes)):
            cell.contentConfiguration = notesConfiguration(for: cell, with: notes)
        default:
            fatalError("Unexpected combination of section and row.")
        }
        cell.tintColor = .todayPrimaryTint
    }
    
    
    @objc func didCancelEdit() {
        workingReminder = reminder
        setEditing(false, animated: true)
    }
    
    private func prepareForEditing() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .cancel, target: self, action: #selector(didCancelEdit))
        updateSnapshotForEditing()
    }
    
    
    private func updateSnapshotForEditing() {
        var snapshot = Snapshot()
        snapshot.appendSections([.title, .date, .notes])
        snapshot.appendItems(
            [.header(Section.title.name), .editableText(reminder.title)], toSection: .title)
        snapshot.appendItems(
            [.header(Section.date.name), .editableDate(reminder.dueDate)], toSection: .date)
        snapshot.appendItems(
            [.header(Section.notes.name), .editableText(reminder.notes)], toSection: .notes)
        dataSource.apply(snapshot)
    }
    
    private func prepareForViewing() {
        navigationItem.leftBarButtonItem = nil
        if workingReminder != reminder {
            reminder = workingReminder
        }
        updateSnapshotForViewing()
    }
    
    private func updateSnapshotForViewing() {
        var snapshot = Snapshot()
        snapshot.appendSections([.view])
        // Because view mode has a single section, it doesn’t need a header.
        snapshot.appendItems(
            [Row.header(""), Row.title, Row.date, Row.time, Row.notes], toSection: .view)
        /*
         Updates the UI to reflect the state of the data in the snapshot, optionally animating
         the UI changes and executing a completion handler.
         */
        dataSource.apply(snapshot)
    }
    
    
    private func section(for indexPath: IndexPath) -> Section {
        /*
         In view mode, all items are displayed in section 0.
         In editing mode, the title, date, and notes are separated into
         sections 1, 2, and 3, respectively.
         */
        let sectionNumber = isEditing ? indexPath.section + 1 : indexPath.section
        guard let section = Section(rawValue: sectionNumber) else {
            fatalError("Unable to find matching section")
        }
        return section
    }
}
